package com.epam.javatraining2016.autoreapirshop;

import java.io.InputStream;
import java.util.Properties;

import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import com.epam.javatraining2016.autoreapirshop.dao.AutoRepairShopServiceDao;

public class SessionFactory {
  // private static final Logger log = LoggerFactory.getLogger(SessionFactory.class);

  private SqlSessionFactory sqlSessionFactory;

  public SessionFactory(String url, String username, String password) {
    SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
    // URL configURL = AutoRepairShopServiceDao.class.getResource("mybatis-config.xml");
    // log.info("{}", configURL);
    InputStream myBatisConfig =
        AutoRepairShopServiceDao.class.getResourceAsStream("mybatis-config.xml");

    Properties properties = new Properties();
    properties.setProperty("jdbc.user", username);
    properties.setProperty("jdbc.password", password);
    properties.setProperty("jdbc.url", url);
    this.sqlSessionFactory = builder.build(myBatisConfig, properties);
  }

  public AutoRepairShopServiceDao getDao() {
    return new AutoRepairShopServiceDao(sqlSessionFactory);
  }
}
