package com.epam.javatraining2016.autoreapirshop;

import java.io.IOException;

import javax.jms.ConnectionFactory;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageType;

@SpringBootApplication
@EnableJms
@EnableCaching
public class Server extends CachingConfigurerSupport {

  @Bean
  SessionFactory sessionFactory(@Value("${jdbc.url}") String url,
      @Value("${jdbc.user}") String username, @Value("${jdbc.password}") String password)
      throws IOException {
    return new SessionFactory(url, username, password);
  }

  // Отличается от того, что делается по умолчанию тем, что ставим MessageConverter в/из JSON
  @Bean
  JmsListenerContainerFactory<?> jmsListenerContainerFactory(ConnectionFactory connectionFactory) {
    DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
      factory.setConnectionFactory(connectionFactory);
      MappingJackson2MessageConverter messageConverter = new MappingJackson2MessageConverter();
      messageConverter.setTargetType(MessageType.TEXT);
      messageConverter.setTypeIdPropertyName("MessageType");
      factory.setMessageConverter(messageConverter);
      return factory;
  }

  public static void main(String[] args) throws InterruptedException {
    SpringApplication application = new SpringApplication(Server.class);
    application.run(args);
  }

}
