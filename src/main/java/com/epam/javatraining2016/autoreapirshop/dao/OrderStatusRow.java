package com.epam.javatraining2016.autoreapirshop.dao;

public class OrderStatusRow extends Row {
  private String statusName = null;

  public OrderStatusRow(Integer id) {
    super(id);
  }

  public String getStatusName() {
    return statusName;
  }

  public void setStatusName(String statusName) {
    this.statusName = statusName;
  }

}
