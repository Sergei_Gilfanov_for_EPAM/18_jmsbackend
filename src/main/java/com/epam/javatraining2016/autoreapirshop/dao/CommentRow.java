package com.epam.javatraining2016.autoreapirshop.dao;

public class CommentRow extends Row {
  private String commentText;

  public CommentRow(Integer id) {
    super(id);
  }

  public String getCommentText() {
    return commentText;
  }

  public void setCommentText(String commentText) {
    this.commentText = commentText;
  }
}
