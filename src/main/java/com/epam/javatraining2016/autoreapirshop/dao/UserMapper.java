package com.epam.javatraining2016.autoreapirshop.dao;

public interface UserMapper {
  void insert(UserRow user);

  UserRow select(int userId);

  UserRow selectByLogin(String login);
}
