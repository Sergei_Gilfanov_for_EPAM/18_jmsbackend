package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;

public interface OrderHistoryRecordCommentMapper {
  void insert(OrderHistoryRecordCommentRow orderHistortyRow);

  OrderHistoryRecordCommentRow selectShallow(int recordId);

  OrderHistoryRecordCommentRow selectDeep(int recordId);

  List<OrderHistoryRecordCommentRow> selectForOrderDeep(int orderId);
}
