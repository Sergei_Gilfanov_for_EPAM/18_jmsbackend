package com.epam.javatraining2016.autoreapirshop.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.apache.ibatis.session.SqlSession;

public class ClientDao {
  private ClientMapper mapper;

  public ClientDao(SqlSession sqlSession) {
    mapper = sqlSession.getMapper(ClientMapper.class);
  }

  public ClientRow create(String name, OrderListRow orderList) {
    ClientRow retval = new ClientRow(0);
    retval.setClientName(name);
    retval.setOrderList(orderList);
    mapper.insert(retval);
    return retval;
  }

  public ClientRow getShallow(int clientId) {
    ClientRow retval = mapper.selectShallow(clientId);
    if (retval == null) {
      throw new NoSuchElementException();
    }
    return retval;
  }

  public List<ClientRow> getAll() {
    List<ClientRow> retval = mapper.selectAll();
    return retval;
  }

  public int delete(int clientId) {
    return mapper.delete(clientId);
  }
}
