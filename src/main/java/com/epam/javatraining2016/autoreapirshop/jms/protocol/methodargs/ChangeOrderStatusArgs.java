package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class ChangeOrderStatusArgs {
  public int id;
  public String statusName;
  public String commentText;

  public ChangeOrderStatusArgs() {
  }

  public ChangeOrderStatusArgs(int id, String statusName, String commentText) {
    this.id = id;
    this.statusName = statusName;
    this.commentText = commentText;
  }

}
