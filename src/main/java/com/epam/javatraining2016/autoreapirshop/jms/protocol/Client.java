package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class Client {

  protected int id;
  protected String clientName;
  protected int orderListId;

  public int getId() {
    return id;
  }

  public void setId(int value) {
    this.id = value;
  }

  public String getClientName() {
    return clientName;
  }

  public void setClientName(String value) {
    this.clientName = value;
  }

  public int getOrderListId() {
    return orderListId;
  }

  public void setOrderListId(int value) {
    this.orderListId = value;
  }

}
