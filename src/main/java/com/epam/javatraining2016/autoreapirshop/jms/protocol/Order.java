package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class Order {

  protected int id;
  protected String status;

  public int getId() {
      return id;
  }

  public void setId(int value) {
      this.id = value;
  }

  public String getStatus() {
      return status;
  }

  public void setStatus(String value) {
      this.status = value;
  }

}
