package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class CreateCommentArgs {
  public int orderId;
  public String commentText;

  public CreateCommentArgs() {
  }

  public CreateCommentArgs(int orderId, String commentText) {
    super();
    this.orderId = orderId;
    this.commentText = commentText;
  }

}
