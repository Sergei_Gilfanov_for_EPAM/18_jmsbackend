package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class OrderHistoryRecordComment
    extends OrderHistoryRecord
{

    protected Comment comment;

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment value) {
        this.comment = value;
    }

}
