package com.epam.javatraining2016.autoreapirshop.jms.protocol;

public class User {

    protected int id;
    protected String login;
    protected String password;
    protected String fullName;
    protected boolean manager;

    public int getId() {
        return id;
    }

    public void setId(int value) {
        this.id = value;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String value) {
        this.login = value;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String value) {
        this.password = value;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String value) {
        this.fullName = value;
    }

    public boolean isManager() {
        return manager;
    }

    public void setManager(boolean value) {
        this.manager = value;
    }

}
