package com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs;

public class GetOrderPagedFilteredArgs {
  public int clientId;
  public int fromOrder;
  public int pageLength;
  public int statusId;

  public GetOrderPagedFilteredArgs() {
  }

  public GetOrderPagedFilteredArgs(int clientId, int fromOrder, int pageLength, int statusId) {
    this.clientId = clientId;
    this.fromOrder = fromOrder;
    this.pageLength = pageLength;
    this.statusId = statusId;
  }
}
