package com.epam.javatraining2016.autoreapirshop.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.epam.javatraining2016.autoreapirshop.api.AutoRepairShopService;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Client;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Comment;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.Order;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrderHistoryRecord;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.OrdersPaged;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.UserSearchResult;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.ChangeOrderStatusArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateCommentArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateOrderArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.CreateUserArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.GetOrderPagedArgs;
import com.epam.javatraining2016.autoreapirshop.jms.protocol.methodargs.GetOrderPagedFilteredArgs;

@Component
public class JmsListeners {
  private static final Logger log = LoggerFactory.getLogger(JmsListeners.class);
  @Autowired
  private AutoRepairShopService service;

  @JmsListener(destination = "getVersion")
  public String getVersion() {
    return service.getVersion();
  }

  @JmsListener(destination = "createClient")
  public int createClient(String name) {
    return service.createClient(name);
  }

  @JmsListener(destination = "getClient")
  public Client getClient(int clientId) {
    log.debug("getClient");
    return service.getClient(clientId);
  }

  @JmsListener(destination = "getClients")
  public Client[] getClients() {
    log.debug("getClients");
    return service.getClients();
  }

  @JmsListener(destination = "deleteClient")
  public int deleteClient(int clientId) {
    return service.deleteClient(clientId);
  }

  @JmsListener(destination = "createComment")
  public int createComment(CreateCommentArgs args) {
    return service.createComment(args.orderId, args.commentText);
  }

  @JmsListener(destination = "getComment")
  public Comment getComment(int commentId) {
    return service.getComment(commentId);
  }

  @JmsListener(destination = "createOrder")
  public int createOrder(CreateOrderArgs args) {
    return service.createOrder(args.clientId, args.commentText);
  }

  @JmsListener(destination = "getOrder")
  public Order getOrder(int id) {
    return service.getOrder(id);
  }

  @JmsListener(destination = "getOrders")
  public Order[] getOrders(int clientId) {
    return service.getOrdersForClient(clientId);
  }

  @JmsListener(destination = "getOrdersPaged")
  public OrdersPaged getOrdersPaged(GetOrderPagedArgs args) {
    return service.getOrdersForClient(args.clientId, args.fromOrder, args.pageLength, null);
  }

  @JmsListener(destination = "getOrdersPagedFiltered")
  public OrdersPaged getOrdersPaged(GetOrderPagedFilteredArgs args) {
    return service.getOrdersForClient(args.clientId, args.fromOrder, args.pageLength, args.statusId);
  }

  @JmsListener(destination = "changeOrderStatus")
  public int changeOrderStatus(ChangeOrderStatusArgs args) {
    return service.changeOrderStatus(args.id, args.statusName, args.commentText);
  }

  @JmsListener(destination = "getHistory")
  public OrderHistoryRecord[] getHistory(int id) {
    OrderHistoryRecord[] retval = service.getHistory(id);
    return retval;
  }

  @JmsListener(destination = "createUser")
  public int createUser(CreateUserArgs args) {
    return service.createUser(args.login, args.fullName, args.password);
  }

  @JmsListener(destination = "getUser")
  public UserSearchResult getUser(String login) {
    UserSearchResult retval = service.getUser(login);
    return retval;
  }

  @JmsListener(destination = "loginAvailable")
  public boolean loginAvailable(String login) {
    boolean retval = service.loginAvailable(login);
    return retval;
  }
}
